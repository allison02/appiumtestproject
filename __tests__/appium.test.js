import wd from 'wd';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
const PORT = 4723;
const config = {
    platformName: 'Android',
    platformVersion: '5.1.1',
    deviceName: 'SM-J111F',
    app: './android/app/build/outputs/apk/app-debug.apk'
};

const driver = wd.promiseChainRemote('localhost', PORT);

beforeAll(async () => {
    await driver.init(config);
    await driver.sleep(2000);
})

test('appium renders', async () => {
    expect(await driver.hasElementByAccessibilityId('testview')).toBe(true);
    expect(await driver.hasElementByAccessibilityId('notthere')).toBe(false);
});